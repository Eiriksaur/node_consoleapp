//Defining the required modules
const fs = require('fs');
const os = require('os');
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World');
  });

//Defining the readline used for input and output
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

//Function that reads a JSON file and logs the content
const readJsonFile = () => {
    console.log('Reading package.json file: ')
    let jsonInput = fs.readFileSync('package.json');
    let jsonFile = JSON.parse(jsonInput);
    console.log(jsonFile);
}

//Function that retrives and logs all OS info
const getOsInfo = () => {
    console.log('Display OS info');
    console.log(`Total memory: ${(os.totalmem()/(1024*1024*1024)).toFixed(2)}GB`)
    console.log(`Free memory: ${(os.freemem()/(1024*1024*1024)).toFixed(2)}GB`);
    console.log(`CPU cores: ${os.cpus().length}`)
    console.log(`ARCH: ${os.arch()}`)
    console.log(`Platform: ${os.platform()}`);
    console.log(`Release: ${os.release()}`);
    console.log(`USER: ${os.userInfo().username}`);
}

//Function that starts a HTTP server at port 3000
const startHTTP = () => {
    server.listen(port, hostname, () => {
        console.log('Starting HTTP server...')
        console.log(`Listening on port:${port}...`);
      });
}

//Displaying the options to the user
console.log('Choose an option:\n1. Read ackage.json\n2. Display OS info\n3. Start HTTP server');

//HAndling the input and choosing the apropriate action
readline.question('Type a number: ', number => {
    if(number === '1'){
        readJsonFile();
    } else if(number === '2'){
        getOsInfo();
    } else if(number === '3'){
        startHTTP();
    } else {
        console.log('Invalid option selected');
    }
    readline.close();
});


